
Due to the short time provided to complete the assignment (two days, when I also needed to work full time), I went forward
with SpringBoot. This provided a simpler way of deploying plus a lot of automagic configuration done through annotations.

Spring-data provided an in-memory data repository, sufficient for tests.

There's a very simple REST interface provided:

/createAccount
Parameters:
initalBalance
Creates an account with a given initial balance

/account
Paramaters:
id
Returns an account with the given id

/transter
Parameters:
fromId
toId
amount
Transfers "amount" from account with id fromId to account with id toId.


Running instructions:
Unit and integration tests:
./gradlew test

Bring up the REST server:
./gradlew bootRun

Test:

curl "http://localhost:8080/createAccount?initialBalance=10000"
curl "http://localhost:8080/createAccount?initialBalance=20000"
curl "http://localhost:8080/transfer?fromId=1&toId=2&amount=5123"


