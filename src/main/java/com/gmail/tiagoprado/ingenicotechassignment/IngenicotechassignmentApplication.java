package com.gmail.tiagoprado.ingenicotechassignment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
//@EnableAsync
public class IngenicotechassignmentApplication {
	public static void main(String[] args) {
		SpringApplication.run(IngenicotechassignmentApplication.class, args);
	}
}
