package com.gmail.tiagoprado.ingenicotechassignment.exceptions;

public class AccountBalanceException extends RuntimeException {
    public AccountBalanceException(String msg) { super(msg); }
}
