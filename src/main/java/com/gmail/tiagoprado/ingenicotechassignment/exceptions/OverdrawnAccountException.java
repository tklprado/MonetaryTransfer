package com.gmail.tiagoprado.ingenicotechassignment.exceptions;

public class OverdrawnAccountException extends RuntimeException {
    public OverdrawnAccountException(String msg) { super(msg); }
}
