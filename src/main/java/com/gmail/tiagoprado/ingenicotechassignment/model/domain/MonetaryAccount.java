package com.gmail.tiagoprado.ingenicotechassignment.model.domain;

import com.gmail.tiagoprado.ingenicotechassignment.exceptions.OverdrawnAccountException;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Simple representation of a monetary account.
 * Contains an auto-generated integer id and a balance (in cents).
 */
@Entity
public class MonetaryAccount {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private Long balance;

    // This is for JPA's sake
    protected MonetaryAccount() {}

    /**
     * Creates a new MonetaryAccount with an initial balance.
     * @param initialBalance Initial account balance, >0
     *
     * @throws RuntimeException if initialBalance is under zero.
     */
    public MonetaryAccount(final Long initialBalance) {
        if (initialBalance < 0L) {
            throw new OverdrawnAccountException("Accounts cannot be created already overdrawn!");
        }
        this.balance = initialBalance;
    }

    @Override
    public String toString() {
        return String.format("MonetaryAccount {id=%d, balance=%d}", id, balance);
    }

    public Long getId() {
        return id;
    }

    public void setBalance(Long balance) {
        this.balance = balance;
    }

    public Long getBalance() {
        return balance;
    }
}
