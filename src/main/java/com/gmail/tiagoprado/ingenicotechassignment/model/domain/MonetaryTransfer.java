package com.gmail.tiagoprado.ingenicotechassignment.model.domain;

import javax.persistence.*;

/**
    Represents a successful monetary transfer between two accounts.
 */
@Entity
public class MonetaryTransfer {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ORIGIN_ACCT_ID")
    private MonetaryAccount originAccount;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DEST_ACCT_ID")
    private MonetaryAccount destinationAccount;

    private Long amount;

    // This is for JPA's sake
    protected MonetaryTransfer() {}

    public MonetaryTransfer(final MonetaryAccount from, final MonetaryAccount to, final Long amount) {
        this.originAccount = from;
        this.destinationAccount = to;
        this.amount = amount;
    }

    @Override
    public String toString() {
        return String.format("MonetaryTransfer {id=%d from=%d, to=%d, amount=%d}",
                id, originAccount.getId(), destinationAccount.getId(), amount);
    }

    public Long getId() {
        return id;
    }

    public MonetaryAccount getOriginAccount() {
        return originAccount;
    }

    public MonetaryAccount getDestinationAccount() {
        return destinationAccount;
    }

    public Long getAmount() {
        return amount;
    }
}
