package com.gmail.tiagoprado.ingenicotechassignment.model.repository;

import com.gmail.tiagoprado.ingenicotechassignment.model.domain.MonetaryAccount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.stereotype.Repository;

import javax.persistence.LockModeType;

import static javafx.scene.input.KeyCode.T;

@Repository
public interface MonetaryAccountRepository extends JpaRepository<MonetaryAccount, Long> {
    @Lock(LockModeType.PESSIMISTIC_WRITE)
    MonetaryAccount findOne(Long id);

    @Lock(LockModeType.PESSIMISTIC_WRITE)
    MonetaryAccount save(MonetaryAccount entity);
}
