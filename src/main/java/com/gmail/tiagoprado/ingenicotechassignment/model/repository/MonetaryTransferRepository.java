package com.gmail.tiagoprado.ingenicotechassignment.model.repository;

import com.gmail.tiagoprado.ingenicotechassignment.model.domain.MonetaryTransfer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MonetaryTransferRepository extends JpaRepository<MonetaryTransfer, Long> {
}
