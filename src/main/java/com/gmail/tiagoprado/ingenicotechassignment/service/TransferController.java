package com.gmail.tiagoprado.ingenicotechassignment.service;

import com.gmail.tiagoprado.ingenicotechassignment.model.domain.MonetaryAccount;
import com.gmail.tiagoprado.ingenicotechassignment.model.domain.MonetaryTransfer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.EntityNotFoundException;

@RestController
public class TransferController {

    @Autowired
    private TransferService service;

    @RequestMapping("/createAccount")
    public MonetaryAccount createAccount(@RequestParam(value="initialBalance") Long initialBalance) {
        return service.createMonetaryAccount(initialBalance);
    }

    @RequestMapping("/account")
    public MonetaryAccount getAccount(@RequestParam(value="id") Long id) {
        MonetaryAccount acc = service.getAccountById(id);
        if (acc == null) {
            throw new EntityNotFoundException("No account with id "+id+"found");
        }
        return acc;
    }

    @RequestMapping("/transfer")
    public MonetaryTransfer transfer(@RequestParam(value="fromId") Long fromId,
                                     @RequestParam(value="toId") Long toId,
                                     @RequestParam(value="amount") Long amount) throws Exception {
        return service.performTransfer(fromId, toId, amount).get();
    }

}
