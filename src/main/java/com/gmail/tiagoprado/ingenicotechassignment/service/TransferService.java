package com.gmail.tiagoprado.ingenicotechassignment.service;

import com.gmail.tiagoprado.ingenicotechassignment.exceptions.OverdrawnAccountException;
import com.gmail.tiagoprado.ingenicotechassignment.model.domain.MonetaryAccount;
import com.gmail.tiagoprado.ingenicotechassignment.model.domain.MonetaryTransfer;
import com.gmail.tiagoprado.ingenicotechassignment.model.repository.MonetaryAccountRepository;
import com.gmail.tiagoprado.ingenicotechassignment.model.repository.MonetaryTransferRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import javax.persistence.LockModeType;
import java.util.concurrent.Future;

@Service
public class TransferService {
    private static final Logger LOGGER = LoggerFactory.getLogger(TransferService.class);

    @Autowired
    private MonetaryAccountRepository accounts;

    @Autowired
    private MonetaryTransferRepository transfers;

    /**
     * REQUIRES_NEW needed primarly for tests - without the propagation anotation, the spawned threads won't see the same
     * transaction that creates the test data.
     * @param initialBalance
     * @return
     */
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public MonetaryAccount createMonetaryAccount(long initialBalance) {
        return accounts.saveAndFlush(new MonetaryAccount(initialBalance));
    }

    public MonetaryAccount getAccountById(Long id) {
        MonetaryAccount ret = accounts.findOne(id);
        LOGGER.debug(ret.toString());
        return ret;
    }

    /**
     * Transfers amount from account fromId to toId.
     * @param fromId origin account id.
     * @param toId destination account id.
     * @param amount amount (in cents).
     * @return future with the TransferRecord created (on success).
     * @throws EntityNotFoundException if any of the accounts don't exist.
     * @throws OverdrawnAccountException if there's not enough balance available to perform the transfer.
     */
    @Transactional
    //@Async
    public Future<MonetaryTransfer> performTransfer(Long fromId, Long toId, Long amount) {
        MonetaryAccount from = accounts.findOne(fromId);
        if (from == null) {
            throw new EntityNotFoundException("Origin account id " + fromId + " not found");
        }

        MonetaryAccount to = accounts.findOne(toId);
        if (to == null) {
            throw new EntityNotFoundException("Destination account id " + toId + " not found");
        }

        if (from.getBalance() < amount) {
            throw new OverdrawnAccountException("Origin account id " + fromId + " balance insufficient for transfer");
        }

        from.setBalance(from.getBalance() - amount);
        to.setBalance(to.getBalance() + amount);
        MonetaryTransfer transfer = new MonetaryTransfer(from, to, amount);

        accounts.save(from);
        accounts.save(to);
        accounts.flush();
        transfers.saveAndFlush(transfer);

        return new AsyncResult<>(transfer);
    }
}
