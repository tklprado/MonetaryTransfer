package com.gmail.tiagoprado.ingenicotechassignment;

import com.gmail.tiagoprado.ingenicotechassignment.exceptions.OverdrawnAccountException;
import com.gmail.tiagoprado.ingenicotechassignment.model.domain.MonetaryAccount;
import com.gmail.tiagoprado.ingenicotechassignment.model.repository.MonetaryAccountRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Simple unit testing for account creation.
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class MonetaryAccountRepositoryTests {
    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private MonetaryAccountRepository accounts;

    @Test
    public void createAndFindById() {
        MonetaryAccount account = new MonetaryAccount(10000L);
        entityManager.persist(account);

        MonetaryAccount findById = accounts.findOne(account.getId());

        assertThat(findById).isEqualToComparingFieldByField(account);
    }

    @Test(expected = OverdrawnAccountException.class)
    public void shouldRaiseExceptionOnNegativeAmount() {
        MonetaryAccount shouldBomb = new MonetaryAccount(-1L);
    }
}
