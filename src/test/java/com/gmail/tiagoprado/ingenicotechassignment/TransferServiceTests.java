package com.gmail.tiagoprado.ingenicotechassignment;

import com.gmail.tiagoprado.ingenicotechassignment.exceptions.AccountBalanceException;
import com.gmail.tiagoprado.ingenicotechassignment.exceptions.OverdrawnAccountException;
import com.gmail.tiagoprado.ingenicotechassignment.model.domain.MonetaryAccount;
import com.gmail.tiagoprado.ingenicotechassignment.model.domain.MonetaryTransfer;
import com.gmail.tiagoprado.ingenicotechassignment.model.repository.MonetaryAccountRepository;
import com.gmail.tiagoprado.ingenicotechassignment.model.repository.MonetaryTransferRepository;
import com.gmail.tiagoprado.ingenicotechassignment.service.TransferService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.*;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Integration test.
 * Creates two test accounts and performs transfers between them.
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class TransferServiceTests {

    @TestConfiguration
    static class TransferServiceTestsConfiguration {
        @Bean
        public TransferService transferService() {
            return new TransferService();
        }
    }

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private TransferService transferService;

    @Autowired
    private MonetaryAccountRepository accounts;

    @Autowired
    private MonetaryTransferRepository transfers;

    private MonetaryAccount to;
    private MonetaryAccount from;

    private static final Long INITIAL_BALANCE = 1000L;
    private static final Long VALID_TRANSFER_AMOUNT = 100L;

    @Before
    public void initTestEnv() {
        to = transferService.createMonetaryAccount(0L);
        from = transferService.createMonetaryAccount(INITIAL_BALANCE);
    }

    @Test(expected = EntityNotFoundException.class)
    public void nonExistantOriginAccountShouldFail() {
        transferService.performTransfer(9999L, to.getId(), VALID_TRANSFER_AMOUNT);
    }

    @Test(expected = EntityNotFoundException.class)
    public void nonExistantDestinationAccountShouldFail() {
        transferService.performTransfer(from.getId(), 9999L, VALID_TRANSFER_AMOUNT);
    }

    @Test(expected = OverdrawnAccountException.class)
    public void overdrawnAccountShouldFail() {
        transferService.performTransfer(from.getId(), to.getId(), INITIAL_BALANCE*2);
    }

    @Test
    public void performTransfer() {
        MonetaryTransfer transfer;
        try {
            transfer = transferService.performTransfer(from.getId(), to.getId(), VALID_TRANSFER_AMOUNT).get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
            transfer = null;
        }

        assertThat(transfer).isNotNull();
        assertThat(transfer.getAmount()).isEqualTo(VALID_TRANSFER_AMOUNT);
        assertThat(accounts.findOne(from.getId()).getBalance()).isEqualTo(INITIAL_BALANCE - VALID_TRANSFER_AMOUNT);
        assertThat(accounts.findOne(to.getId()).getBalance()).isEqualTo(VALID_TRANSFER_AMOUNT);
    }


    /**
     * Transfers all balance from one account to another in various serial transactions.
     */
    @Test
    public void serialTest() {
        int successes = 0;
        int failures = 0;
        for(int i=0; i<20; i++) {
            MonetaryTransfer transfer = null;
            try {
                transfer = transferService.performTransfer(from.getId(), to.getId(), VALID_TRANSFER_AMOUNT).get();
            } catch (InterruptedException|ExecutionException e) {
                e.printStackTrace();
                failures++;
            } catch (OverdrawnAccountException e) {
                failures++;
            }
            if (transfer != null) {
                successes++;
            }
        }

        assertThat(successes).isEqualTo(10);
        assertThat(failures).isEqualTo(10);
    }

    class TransferExecutionResult {
        private Exception thrownException;
        private MonetaryTransfer transfer;
        private Long attemptedAmount = 0L;

        public TransferExecutionResult(Exception e, Long attemptedAmount) {
            thrownException = e;
            this.attemptedAmount = attemptedAmount;
        }
        public TransferExecutionResult(MonetaryTransfer t) {
            transfer = t;
        }

        public boolean isSuccessful() {
            return transfer != null;
        }

        public Exception getThrownException() {
            return thrownException;
        }

        public Long getAttemptedAmount() {
            return attemptedAmount;
        }

        public MonetaryTransfer getTransfer() {
            return transfer;
        }
    }

    class SingleTransfer implements Callable<TransferExecutionResult> {
        Long fromId, toId, amount;
        public SingleTransfer(Long fromId, Long toId, Long amount) {
            this.fromId = fromId;
            this.toId = toId;
            this.amount = amount;
        }

        @Override
        public TransferExecutionResult call() {
            try {
                MonetaryTransfer transfer = transferService.performTransfer(fromId, toId, amount).get();
                return new TransferExecutionResult(transfer);
            } catch (Exception e) {
                return new TransferExecutionResult(e, amount);
            }
        }
    }

    /**
     * Creates 50 jobs trying to perform transfers from the same two accounts.
     * Should fail when there's no balance to transfer from.
     * At each step, the balance sum should be equal to the total initial balance.
     */
    @Test
    public void concurrentTest() {
        ExecutorService executor = Executors.newFixedThreadPool(8);

        List<Callable<TransferExecutionResult>> tasks = new ArrayList<>();
        for (int i = 0; i<50; i++) {
            tasks.add(new SingleTransfer(from.getId(), to.getId(),
                    Math.round(Math.random()*VALID_TRANSFER_AMOUNT)));
        }

        int transferTotal = 0;
        try {
            // Start all jobs
            List<Future<TransferExecutionResult>> results = executor.invokeAll(tasks);
            for(Future<TransferExecutionResult> result: results) {
                // Get is the blocking call; this ensures this loop will wait for all the Callables to finish.
                TransferExecutionResult transferResult = result.get();
                if (transferResult.isSuccessful()) {
                    transferTotal += transferResult.getTransfer().getAmount();
                }
            }
        }
        catch (InterruptedException|ExecutionException e) {
            e.printStackTrace();
            transferTotal = -1;
        }

        executor.shutdown();

        MonetaryAccount toAtFinish = transferService.getAccountById(to.getId());
        MonetaryAccount fromAtFinish = transferService.getAccountById(from.getId());

        Long[] result = {toAtFinish.getBalance()+fromAtFinish.getBalance(),
                toAtFinish.getBalance() - transferTotal,
                fromAtFinish.getBalance() + transferTotal
        };
        assertThat(result).containsExactly(INITIAL_BALANCE, 0L, INITIAL_BALANCE);
    }

}
